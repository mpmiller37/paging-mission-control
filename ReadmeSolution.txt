Programming Challenge selected:  Paging Mission Control

Solution using Java / Maven

Solution artifact is an executable jar file accepting a single argument which is the input
file to be processed. To execute junit tests and build jar enter the following command in the
folder containing the pom.xml file:
- mvn clean install

To execute the jar file:
- cd target    // the jar file will be in the target folder
- java -jar mission-control-0.0.1-SNAPSHOT.jar ..\testData\SampleData.txt

Output will appear in console.  Besides the sample data included in requirements, there are
3 other test files:
MultipleSatsBattery.txt - 2 sats (1000, 5000) containing RED LOW battery alerts within 5 minutes 
MultipleSatsThermo.txt -  2 sats (1000, 5000) containing RED HIGH thermostat alerts within 5 minutes
MultipleBatteryAndThermo.txt -  2 sats (1000, 5000) containing battery and thermostat alerts within 5 minutes


