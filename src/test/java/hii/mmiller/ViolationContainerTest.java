package hii.mmiller;

import static org.junit.Assert.*;

import org.junit.Test;

import hii.mmiller.input.BatteryReading;
import hii.mmiller.input.SatReading;
import hii.mmiller.input.ThermoReading;

public class ViolationContainerTest {

	@Test
	public void test3BattViolationsNotWithin5() {
		
		// 3 Battery violations outside 5 minutes
		String line1 = "20230101 20:01:01.000|1000|17|15|9|8|7.0|BATT";	// violation
		String line2 = "20230101 20:01:02.000|1000|17|15|9|8|6.0|BATT";	// violation
		String line3 = "20230101 20:06:03.000|1000|17|15|9|8|5.0|BATT";	// violation
		
		ViolationContainer vc = null;
		BatteryReading satReading = new BatteryReading(line1);
		assertTrue(satReading.limitViolation());
		vc = new ViolationContainer(satReading);
		
		satReading = new BatteryReading(line2);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 2);
		
		// the last violation NOT within 5 minutes, so the container starts over with this as first
		satReading = new BatteryReading(line3);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 1);
	}
	
	@Test
	public void test3BattViolationsWithin5() {
		
		// 3 Battery violations within 5 minutes
		String line1 = "20230101 20:01:01.000|1000|17|15|9|8|7.0|BATT";	// violation
		String line2 = "20230101 20:01:02.000|1000|17|15|9|8|6.0|BATT";	// violation
		String line3 = "20230101 20:01:03.000|1000|17|15|9|8|5.0|BATT";	// violation
		
		ViolationContainer vc = null;
		BatteryReading satReading = new BatteryReading(line1);
		assertTrue(satReading.limitViolation());
		vc = new ViolationContainer(satReading);
		
		satReading = new BatteryReading(line2);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 2);
		
		satReading = new BatteryReading(line3);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 3);
		
		// 3 violations within 5 minutes, get Alert data
		SatReading alert = vc.getAlert();
		assertTrue(alert.getSatId().equals("1000"));
		assertTrue(alert.getSeverity().equals("RED LOW"));
		assertTrue(alert.getComponent().equals("BATT"));
		String initialTime = SatReading.sdf.format(alert.getReadTime());
		assertTrue(initialTime.equals("20230101 20:01:01.000"));
	}
	
	@Test
	public void test3ThermoViolationsNotWithin5() {
		
		// 3 Thermo violations outside 5 minutes
		String line1 = "20230101 20:01:01.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		String line2 = "20230101 20:01:02.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		String line3 = "20230101 20:06:03.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		
		ViolationContainer vc = null;
		ThermoReading satReading = new ThermoReading(line1);
		assertTrue(satReading.limitViolation());
		vc = new ViolationContainer(satReading);
		
		satReading = new ThermoReading(line2);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 2);
		
		// the last violation NOT within 5 minutes, so the container starts over with this as first
		satReading = new ThermoReading(line3);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		System.out.println("violations: " + vc.getNumberOfViolations());
		assertEquals(vc.getNumberOfViolations(), 1);
	}
	
	@Test
	public void test3ThermoViolationsWithin5() {
		
		// 3 Thermo violations outside 5 minutes
		String line1 = "20230101 20:01:01.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		String line2 = "20230101 20:01:02.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		String line3 = "20230101 20:04:03.000|1000|101|98|25|20|102.9|TSTAT";	// violation
		
		ViolationContainer vc = null;
		ThermoReading satReading = new ThermoReading(line1);
		assertTrue(satReading.limitViolation());
		vc = new ViolationContainer(satReading);
		
		satReading = new ThermoReading(line2);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 2);
		
		// the last violation NOT within 5 minutes, so the container starts over with this as first
		satReading = new ThermoReading(line3);
		assertTrue(satReading.limitViolation());
		vc.addViolation(satReading);
		assertEquals(vc.getNumberOfViolations(), 3);
		
		// 3 violations within 5 minutes, get Alert data
		SatReading alert = vc.getAlert();
		assertTrue(alert.getSatId().equals("1000"));
		assertTrue(alert.getSeverity().equals("RED HIGH"));
		assertTrue(alert.getComponent().equals("TSTAT"));
		String initialTime = SatReading.sdf.format(alert.getReadTime());
		assertTrue(initialTime.equals("20230101 20:01:01.000"));
	}
	

}
