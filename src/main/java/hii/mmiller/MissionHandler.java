package hii.mmiller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import hii.mmiller.input.SatFactory;
import hii.mmiller.input.SatReading;

/**
 * Handles the reading of input and processing the sat reading data.
 */
public class MissionHandler {
	
	// list containing sat reading from file
	List<SatReading> satReadings = new ArrayList<SatReading>();
	
	public MissionHandler() {}
	
	/**
	 * Read from input file
	 * @param path Absolute path to file 
	 */
	public void readMissionData(String path) {
		
		BufferedReader br = null;
		String line = null;
		try {
			
			br = new BufferedReader(new FileReader(path));
			while((line = br.readLine()) != null) {
				SatReading satRead = SatFactory.createReading(line);
				satReadings.add(satRead);
			}
			
		} catch (FileNotFoundException e1) {
			System.out.println("File: " + path + " was not found.");
			return;
		}
		catch (IOException e2) {
			System.out.println("Error reading file: " + e2.getMessage());
			return;
		}
		finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("Error closing input file: " + e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Process the sat data
	 */
	public void processMission() {

		// These maps will hold violation data.  There is a Map for each component type containing those
		// types of violations, and the maps are keyed based on sat id.
		Map<String, ViolationContainer> battViolationMap = new HashMap<String, ViolationContainer>();
		Map<String, ViolationContainer> thermoViolationMap = new HashMap<String, ViolationContainer>();
		
		// List of sat alert to be output
		List<SatReading> outputAlerts = new ArrayList<SatReading>();
		
		Map<String, ViolationContainer> currViolationMap = null;
		
		// loop through sat readings only paying attention to those containing violations
		for(SatReading satReading: satReadings) {

			if(!satReading.limitViolation()) {
				continue;
			}
			
			// based on component type, determine which violation map will be used
			if(satReading.getComponent().equals(SatReading.BATTERY)) {
				currViolationMap = battViolationMap;
			} else if(satReading.getComponent().equals(SatReading.THERMOSTAT)) {
				currViolationMap = thermoViolationMap;
			}
			
			// get the Violationcontainer from the map based on Sat id, if it doesn't exist, create one, otherwise
			// add the violation to the container and check to see if an alert needs to be generated
			String satId = satReading.getSatId();
			ViolationContainer vcontainer = currViolationMap.get(satId);
			if(vcontainer == null) {
				vcontainer = new ViolationContainer(satReading);
				currViolationMap.put(satId, vcontainer);
			}
			else {
				vcontainer.addViolation(satReading);
				if(vcontainer.getNumberOfViolations() == 3) {
					// output alert
					outputAlerts.add(vcontainer.getAlert());
					// remove the container for this sat id
					currViolationMap.remove(satId);
				}
			}
				
		}
		
		// output alerts found
		consoleOutput(outputAlerts);
		
	}
	
	/**
	 * Output alerts to console in json
	 * @param alerts
	 */
	private void consoleOutput(List<SatReading> alerts) {
	
		// creating the ObjectMapper object
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);

		// convert the Object to json
		String jsonString = "empty";
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alerts);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Error generating json output: " + e.getMessage());
		}
		System.out.println(jsonString);
	}
}
