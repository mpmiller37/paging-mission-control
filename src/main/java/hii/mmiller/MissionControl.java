package hii.mmiller;

/**
 * Main class.  Accepts argument to the input file and invokes MissionHandler methods.
 */
public class MissionControl {

	public static void main(String[] args) {
		
		if(args.length == 0) {
			System.out.println("Input data file path required as input.");
			return;
		}
		
		String path = args[0];
		
		MissionHandler handler = new MissionHandler();
		handler.readMissionData(path);
		handler.processMission();
	}
}
