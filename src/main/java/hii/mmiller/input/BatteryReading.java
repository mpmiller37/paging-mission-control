package hii.mmiller.input;

/**
 * Battery Reading
 */
public class BatteryReading extends SatReading {

	public BatteryReading(String line) {
		super(line);
	}

	@Override
	public boolean limitViolation() {

		if(this.getRawValue() < this.getRedLowLimit()) {
			return true;
		}
		return false;
	}

	@Override
	public String getSeverity() {
		return "RED LOW";
	}

}
