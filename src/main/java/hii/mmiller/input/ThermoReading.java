package hii.mmiller.input;

/**
 * Thermostat reading
 */
public class ThermoReading extends SatReading{

	public ThermoReading(String line) {
		super(line);
	}

	@Override
	public boolean limitViolation() {

		if(this.getRawValue() > this.getRedHighLimit()) {
			return true;
		}
		return false;
	}

	@Override
	public String getSeverity() {
		return "RED HIGH";
	}

}
