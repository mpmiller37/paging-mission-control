package hii.mmiller.input;

/**
 * Create a subclass of SatReading based on compoant type.
 */
public class SatFactory {

	protected static final int NUM_PARTS = 7;
	
	public static SatReading createReading(String line) {
		
		String[] parts = line.split("\\|");
		String componentType = parts[NUM_PARTS];
		
		if(componentType.equals(SatReading.BATTERY)) {
			return new BatteryReading(line);
		} else if(componentType.equals(SatReading.THERMOSTAT)) {
			return new ThermoReading(line);
		}
		
		throw new RuntimeException("Invalid component type: " + componentType);
	}
}
