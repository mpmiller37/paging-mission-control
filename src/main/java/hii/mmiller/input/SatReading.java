package hii.mmiller.input;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Abstract base class for sat readings.  This class contains many json annotations used to control
 * and or format the json output.
 */
@JsonPropertyOrder({ "satId", "severity", "component", "readTime" })
public abstract class SatReading {

		public static final String BATTERY = "BATT";
		public static final String THERMOSTAT = "TSTAT";
	
		private String satId;
		private Date readTime;
		
		private int redHighLimit;
		private int yelHighLimit;
		private int yelLowLimit;
		private int redLowLimit;
		private float rawValue;
		@JsonProperty
		private String component;
		
		public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

		public SatReading(String line) {
			
			String[] parts = line.split("\\|");
			try {
				readTime = sdf.parse(parts[0]);
			} catch (ParseException e) {
				throw new RuntimeException("Invalid timestamp: " + parts[0]);
			}
			
			satId = parts[1];
			redHighLimit = Integer.parseInt(parts[2]);
			yelHighLimit = Integer.parseInt(parts[3]);
			yelLowLimit = Integer.parseInt(parts[4]);
			redLowLimit = Integer.parseInt(parts[5]);
			rawValue = Float.parseFloat(parts[6]);
			component= parts[7];
		}
		
		public abstract boolean limitViolation();
		@JsonProperty
		public abstract String getSeverity();
		
		@JsonProperty("timestamp")
		@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "EST")
		public Date getReadTime() {
			return readTime;
		}

		@JsonProperty("satelliteId")
		public String getSatId() {
			return satId;
		}

		public int getRedHighLimit() {
			return redHighLimit;
		}

		public int getYelHighLimit() {
			return yelHighLimit;
		}

		public int getYelLowLimit() {
			return yelLowLimit;
		}

		public int getRedLowLimit() {
			return redLowLimit;
		}

		public float getRawValue() {
			return rawValue;
		}

		public String getComponent() {
			return component;
		}
		
		public String toString() {
			return "Id: " + this.getSatId() + " Severity: " + this.getSeverity() + " Component: " + this.getComponent() +
					" Timestamp: " +  sdf.format(getReadTime());
		}
		
}
