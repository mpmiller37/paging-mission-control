package hii.mmiller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hii.mmiller.input.SatReading;

/**
 * Stores violation data for a set of sat readings for a particular sat
 */
public class ViolationContainer {

	// 5 minute duration
	private long MAX_DURATION_MILLI = 5*60*1000;
	
	private Date initialTime = null;
	private int numberOfViolations = 0;
	
	// sat readings with violations 
	List<SatReading> readings = new ArrayList<SatReading>();
	
	/**
	 * Constructor is passed the initial violation
	 * @param reading
	 */
	public ViolationContainer(SatReading reading) {
		
		initialTime = reading.getReadTime();
		++numberOfViolations;
		readings.add(reading);
	}
	
	/**
	 * Add a violation to this container.  If the violation is within 5 minutes, bump violation count and store it,
	 * otherwise reset the container such that this is the first violation found.
	 * @param reading
	 */
	public void addViolation(SatReading reading) {
		
		if((reading.getReadTime().getTime() - initialTime.getTime()) < MAX_DURATION_MILLI) {
			++numberOfViolations;
			readings.add(reading);
			return;
		}
		
		// if we get here, the time difference for this new violation was beyond 5 minutes
		// so start over with this new violation
		initialTime = reading.getReadTime();
		numberOfViolations = 1;
		readings.clear();
		readings.add(reading);
	}
	
	public int getNumberOfViolations() {
		return this.numberOfViolations;
	}
	
	/**
	 * Returns the initial sat reading in the list.  The user invokes this when the violation count
	 * reaches a threshold.  The alert data needed is within the SatReading object for the first
	 * violation.
	 * @return
	 */
	public SatReading getAlert() {
		
		// get initial sat reading
		SatReading firstOne = readings.get(0);
		return firstOne;
	}
}
